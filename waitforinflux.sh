#!/bin/bash

echo "Waiting for InfluxDB"

until $(curl --output /dev/null --silent --fail http://influxdb1:8086/query?q=SHOW+DATABASES); do
    printf '.'
    sleep 5
done

echo InfluxDB ready