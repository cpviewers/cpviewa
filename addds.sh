#!/bin/bash

json='
  {
    "name": "cpview",
    "type": "influxdb",
    "typeLogoUrl": "public/app/plugins/datasource/influxdb/img/influxdb_logo.svg",
    "access": "proxy",
    "url": "http://influxdb1:8086",
    "password": "",
    "user": "",
    "database": "cpview",
    "basicAuth": false,
    "basicAuthUser": "",
    "basicAuthPassword": "",
    "withCredentials": false,
    "isDefault": true
  }
}
'

echo $json | curl -0 -v -X POST http://admin:admin@grafana1:3000/api/datasources \
-H 'Content-Type: text/json; charset=utf-8' \
-d @-
