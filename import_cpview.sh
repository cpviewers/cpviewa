#!/bin/bash

chmod +x ./csv-to-influxdb

INFLUXDB_HOST="influxdb1"
#CPU HISTORY
sqlite3 -csv -header /in/CPViewDB.dat  "SELECT datetime(Timestamp,'unixepoch','localtime') as ts, name_of_cpu, cpu_usage FROM UM_STAT_UM_CPU_UM_CPU_TABLE ORDER BY Timestamp" > /in/cpu.csv
./csv-to-influxdb -s http://${INFLUXDB_HOST}:8086 -d cpview -m cpu -t name_of_cpu -ts ts /in/cpu.csv
#AVERAGE CPU
sqlite3 -csv -header /in/CPViewDB.dat  "SELECT datetime(Timestamp, 'unixepoch', 'localtime') as ts,avg(cpu_usage) as 'average CPU' FROM main.UM_STAT_UM_CPU_UM_CPU_ORDERED_TABLE group by ts" > /in/avgcpu.csv
./csv-to-influxdb -s http://${INFLUXDB_HOST}:8086 -d cpview -m avgcpu -ts ts /in/avgcpu.csv
#FW COUNTERS
sqlite3 -csv -header /in/CPViewDB.dat  "SELECT datetime(Timestamp,'unixepoch','localtime') as ts, inbound_throughput, concurrent_conns, conn_rate, accel_inbound_throughput,pxl_inbound_throughput,fw_inbound_throughput FROM fw_counters" > /in/fwcounters.csv
./csv-to-influxdb -s http://${INFLUXDB_HOST}:8086 -d cpview -m fwcounters -ts ts /in/fwcounters.csv
#MEMORY STATS
sqlite3 -csv -header /in/CPViewDB.dat  "SELECT datetime(Timestamp,'unixepoch','localtime') as ts, Timestamp, real_total,real_used,swap_used,real_free FROM UM_STAT_UM_MEMORY" > /in/mem_stats.csv
./csv-to-influxdb -s http://${INFLUXDB_HOST}:8086 -d cpview -m mem_stats -ts ts /in/mem_stats.csv
#NIC STATS DROPS & ERRORS PER INTERFACE
sqlite3 -csv -header /in/CPViewDB.dat  "SELECT datetime(timestamp,'unixepoch','localtime') as ts, if_name,if_rx_drops,if_rx_errors,if_tx_drops,if_tx_errors FROM UM_STAT_UM_HW_UM_IF_ERR_STATISTICS_TABLE" > /in/nic_stats.csv
./csv-to-influxdb -s http://${INFLUXDB_HOST}:8086 -d cpview -m nic_stats -t if_name -ts ts /in/nic_stats.csv
#NIC STATS DROPS & ERRORS TOTAL
sqlite3 -csv -header /in/CPViewDB.dat  "SELECT datetime(timestamp,'unixepoch','localtime') as ts, if_rx_drops,if_rx_errors,if_tx_drops,if_tx_errors FROM UM_STAT_UM_HW_UM_IF_ERR_STATISTICS_TABLE" > /in/nic_stats_total.csv
./csv-to-influxdb -s http://${INFLUXDB_HOST}:8086 -d cpview -m nic_stats_total -ts ts /in/nic_stats_total.csv
# mem fail
sqlite3 -csv -header /in/CPViewDB.dat  "SELECT datetime(timestamp,'unixepoch','localtime') as ts, failed_alloc FROM kernel_memory_kmem" > /in/kernel_memory_kmem.csv
./csv-to-influxdb -s http://${INFLUXDB_HOST}:8086 -d cpview -m kernel_memory_kmem -ts ts /in/kernel_memory_kmem.csv

sqlite3 -csv -header /in/CPViewDB.dat  "SELECT datetime(timestamp,'unixepoch','localtime') as ts, failed_alloc FROM kernel_memory_hmem" > /in/kernel_memory_hmem.csv
./csv-to-influxdb -s http://${INFLUXDB_HOST}:8086 -d cpview -m kernel_memory_hmem -ts ts /in/kernel_memory_hmem.csv

sqlite3 -csv -header /in/CPViewDB.dat  "SELECT datetime(timestamp,'unixepoch','localtime') as ts, failed_alloc FROM kernel_memory_smem" > /in/kernel_memory_smem.csv
./csv-to-influxdb -s http://${INFLUXDB_HOST}:8086 -d cpview -m kernel_memory_smem -ts ts /in/kernel_memory_smem.csv

sqlite3 -csv -header /in/CPViewDB.dat  "SELECT datetime(timestamp,'unixepoch','localtime') as ts, used_virt_mem, used_virt_mem_percentage FROM fw_memory" > /in/fwmem.csv
./csv-to-influxdb -s http://${INFLUXDB_HOST}:8086 -d cpview -m fwmem -ts ts /in/fwmem.csv
